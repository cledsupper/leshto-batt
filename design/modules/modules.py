#!/usr/bin/python3
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

# Classe genérica
class Module():
    def __init__(self, path='', name='module', ext='.ui'):
        self.builder = Gtk.Builder()
        self.builder.add_from_file(path + name + ext)
        self.widget = self.builder.get_object(name)
        # O nome do arquivo deve ser o mesmo do widget

# Janela do app modular, o módulo principal
class ModulesWindow(Module):
    def __init__(self):
        super().__init__(name='modules-window')
        
        stack = self.builder.get_object('stack1')
        # Nenhum outro widget é relevante
        self.builder = None
        
        # Módulo A
        mod = Module(name='ma')
        # Botão que interage com widget de outro módulo
        self.ma_btn = mod.builder.get_object('ma-btn')
        # Adiciona o widget do Módulo A na pilha
        stack.add_titled(mod.widget, 'ma', 'Módulo A')
        
        # Porque o Módulo A apenas contém widgets, não é relevante
        # ... mantê-lo na memória.
        # Módulo B
        mod = Module(name='mb')
        # Texto que será alterado pelo outro módulo
        self.mb_label = mod.builder.get_object('mb-label')
        stack.add_titled(mod.widget, 'mb', 'Módulo B')
        
        # Lógica de funcionamento do ModulesWindow
        self.ma_btn_clicks = 0
        self.ma_btn.connect('clicked', self.on_ma_btn_click)

    # Simplifica self.mw.window.show_all() na classe do app
    def show_all(self):
        self.widget.show_all()

    def on_ma_btn_click(self, btn):
       self.ma_btn_clicks += 1
       self.mb_label.set_text('O botão do Módulo A foi clicado %d vez(es)' %
           self.ma_btn_clicks)

# Isto NÃO é um módulo, é o nosso aplicativo, que pode ser substituído por
# ... Gtk.Application
class ModulesApp():
    def __init__(self):
        self.mw = None

    def build(self):
        if self.mw is None:
            self.mw = ModulesWindow()
            self.mw.widget.connect('destroy', self.quit)
            self.mw.show_all()

    def run(self):
        self.build()
        Gtk.main()

    def quit(self, win):
        Gtk.main_quit()

if __name__ == '__main__':
    app = ModulesApp()
    app.run()
