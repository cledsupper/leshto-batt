### LESHTO BATT

## Um software de bateria mais inteligente para o Linux

### Testando e instalando.
 - 0. Para testar, execute o arquivo src/runner.py;

 - 1. Instale o arquivo lebatt.desktop em ~/.local/share/applications;

 - 2. Crie um atalho/link simbólico para o arquivo runner.py em ~/.local/bin (pode ser necessário criar), nomeado "lebatt" (sem aspas);

 - 3. Confirme que o script runner.py possui permissão de execução;

 - 4. Reinicie a sessão caso o diretório ~/.local/bin tenha sido criado agora;

 - 5. Pesquise 'lebatt' no lançador de aplicativos do sistema.

## Apoie os Leshto Apps!

Se você quiser apoiar o desenvolvimento do Leshto Battery (e de outros aplicativos meus), "invista" neste projeto pagando acima de R$ 9,99 para a chave Pix abaixo, com a mensagem "Leshto-Apps":

> 54902406-dba4-40cc-a8ae-6ee696f7f5c6

```
Confirme a conta:

Instituição: NU PAGAMENTOS - IP
Agência: 0001
Conta: 662065-9
Nome: CLEDSON FERREIRA CAVALCANTI
```

Eu me coloco à disposição de devolver o seu dinheiro em até 30 dias a contar da data do pagamento, desde que você faça a solicitação educadamente via e-mail com a hashtag `#REFUND Leshto-Apps` no assunto, e o seu nome completo no corpo do e-mail. O reembolso pode ocorrer, SE EU QUISER e PUDER, dentro de 7 dias, como devolução Pix.

AVISO MAIS IMPORTANTE AINDA: NÃO HÁ GARANTIA DE DINHEIRO DE VOLTA (REEMBOLSO, ESTORNO, DEVOLUÇÃO, "CASHBACK"m "REFUND")! DOE POR SUA PRÓPRIA CONTA E RISCO.
