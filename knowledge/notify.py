#!/usr/bin/python3
import gi
gi.require_version('Notify', '0.7')
from gi.repository import Notify

Notify.init('com.gitlab.cledsupper.LeshtoBatt')

notify = Notify.Notification.new(
    'Leshto Batt Test',
    'Teste de notificação do Leshto Battery',
    None
)
notify.set_timeout(10000)
notify.show()

Notify.uninit()
