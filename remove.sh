#!/bin/bash

ID_USUARIO=0
if [[ $(id -u) -eq $ID_USUARIO ]]; then
  echo "Não desinstale em modo root OU troque a ID_USUARIO do script para um número diferente de 0!"
  exit 0
fi

echo "Desinstalando..."
if [[ "$(whereis lebatt | awk '{print $2}')" = "$HOME/.local/bin/lebatt" ]]; then
    rm $HOME/.local/bin/lebatt
    if [[ -L "$HOME/.local/opt/leshto-batt" ]]; then
        rm $HOME/.local/opt/leshto-batt
    else
        rm -r $HOME/.local/opt/leshto-batt
    fi
    rm $HOME/.local/share/applications/lebatt.desktop
else
    echo "Instalação manual detectada: remova-o manualmente conforme o código deste script!"
fi

if [[ -f /usr/local/bin/battlimiter-iface.sh ]]; then
    echo "Para desinstalar o plugin de conservação de bateria, é necessário se autenticar como super usuário."
    sudo rm /usr/local/bin/battlimiter-iface.sh
    if [[ $? -ne 0 ]]; then
        echo "Erro ao desinstalar o plugin!"
        echo "Desinstale-o manualmente com o comando:"
        echo "  $ sudo rm /usr/local/bin/battlimiter-iface.sh"
    else
        echo "Desativar serviço"
        sudo systemctl stop battlimiter.service
        sudo systemctl disable battlimiter.service
        sudo rm /etc/systemd/system/battlimiter.service
        sudo rm /usr/local/bin/battlimiter-service.sh
    fi
fi
echo "OK"
echo "por tales"
