# messages.py - Grandes mensagens que podem ser exibidas pelo LeshtoBattery
# Utilizado pelos módulos:
# -> lib.batttweaker

ERROR_SETTINGS_TITLE = 'LeshtoBatt: o arquivo de configuração ["%s"] está corrompido'
ERROR_SETTINGS_MSG = """1. Se escreveu comentários, as linhas devem começar com # ou ;
2. As chaves não podem ter espaços nos nomes.
3. Sintaxe correta:
         ['secao']
	 chave_booleana = True
	 chave_inteira = 0
	 chave_real = 24.5
	 chave_frase = Minha string bonita e maravilhosa
4. As chaves são separadas como no exemplo acima: quebras de linha
"""
