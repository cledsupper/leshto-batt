#   Leshto Batt batt.py - Interface de leitura dos dados da bateria
#   Copyright (C) 2021-2024 Cledson Ferreira <cledsonitgames@gmail.com>

#    This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
import os
from typing import Union

def f_value(v: float) -> float:
    return v/1000000.0

def f_temp(v: float) -> float:
    return v/10.0

def read_value(dirpath: str, filename: str) -> str:
    """Lê a primeira linha do arquivo sem a quebra de linha"""
    value = ''
    with open(os.path.join(dirpath, filename)) as f:
        value = f.readline()
        value = value[:len(value)-1]
    return value

def read_check(dirpath: str, filename: str, text: str) -> bool:
    """Verifica se a primeira linha = text"""
    try:
        value = read_value(dirpath, filename)
        return text == value
    except FileNotFoundError:
        return False

class Battery:
    """Classe Battery para acessar informações da bateria"""

    PATH = '/sys/class/power_supply/BAT0/'

    def __init__(self, dirpath: str = PATH, check_unit: bool = True):
        if not read_check(dirpath, 'type', 'Battery'):
            raise TypeError(
"""Battery('%s'): endereço não aponta para uma bateria válida.
    CORRIJA-ME: um endereço de bateria sysfs válido possui um arquivo 'type'
com o texto 'Battery'.""" % (dirpath))
        self.path = dirpath
        
        self._technology: Union[str, None] = None
        # Valores podem ser: 'W', 'A' ou '?'
        self._unit = '?'
        self._unit_checked = False
        if check_unit:
            self.get_unit()

    def get_unit(self) -> str:
        if self._unit_checked:
            return self._unit
        self._unit_checked = True
        
        print('Battery: checando unidade de carga...')
        ls = os.listdir(self.path)
        for name in ls:
            print(' -> "' + name + '"')
            if 'energy' in name or 'power' in name:
                self._unit = 'W'
            elif 'charge' in name or 'current' in name:
                self._unit = 'A'
            else:
                self._unit = '?'
                continue
            break
        else:
            print('Battery: a bateria não reporta valores de carga.')
            print('Battery: os métodos capacity, charge e current não estão disponíveis')
        return self._unit

    def percent(self) -> int:
        """Nível de carga da bateria em percentual"""
        return int(read_value(self.path, 'capacity'))

    def charging(self) -> bool:
        """A bateria está carregando?"""
        return read_check(self.path, 'status', 'Charging')

    def capacity(self) -> Union[float, None]:
        """Retorna a capacidade estimada da bateria em Watts ou Amperes"""
        unit = self.get_unit()
        try:
            if unit == 'A':
                v = float(read_value(self.path, 'charge_full'))
                return f_value(v)
            elif unit == 'W':
                v = float(read_value(self.path, 'energy_full'))
                return f_value(v)
        except:
            pass
        return None

    def capacity_design(self) -> Union[float, None]:
        """Retorna a capacidade típica da bateria em Watts ou Amperes"""
        unit = self.get_unit()
        try:
            if unit == 'A':
                v = float(read_value(self.path, 'charge_full_design'))
                return f_value(v)
            elif unit == 'W':
                v = float(read_value(self.path, 'energy_full_design'))
                return f_value(v)
        except:
            pass
        return None

    def energy_now(self) -> Union[float, None]:
        """Retorna o nível de carga da bateria em Watts ou Amperes""" 
        unit = self.get_unit()
        try:
            if unit == 'A':
                v = float(read_value(self.path, 'charge_now'))
                return f_value(v)
            elif unit == 'W':
                v = float(read_value(self.path, 'energy_now'))
                return f_value(v)
        except:
            pass
        return None

    def current_now(self) -> Union[float, None]:
        """Retorna a velocidade da (des)carga em Watts ou Amperes""" 
        unit = self.get_unit()
        try:
            if unit == 'A':
                try:
                    v = float(read_value(self.path, 'current_avg'))
                except:
                    v = float(read_value(self.path, 'current_now'))
                return f_value(v)
            elif unit == 'W':
                try:
                    v = float(read_value(self.path, 'power_avg'))
                except:
                    v = float(read_value(self.path, 'power_now'))
                return f_value(v)
        except:
            pass
        return None

    def temp(self) -> Union[float, None]:
        """Temperatura da bateria (ºC)"""
        try:
            return f_temp(float(read_value(self.path, 'temp'))/10.0)
        except:
            pass
        return None

    def voltage(self) -> Union[float, None]:
        """Tensão da bateria (V)"""
        v = None
        try:
            v = float(read_value(self.path, 'voltage_avg'))
        except:
            try:
                v = float(read_value(self.path, 'voltage_now'))
            except:
                pass
        return f_value(v) if v is not None else None

    def health(self) -> Union[str, None]:
        """Saúde da bateria"""
        try:
            return read_value(self.path, 'health')
        except:
            return None

    def technology(self) -> Union[str, None]:
        """Tecnologia da bateria (Li-ion, Li-poly, etc.)"""
        if self._technology is None:
            try:
                self._technology = read_value(self.path, 'technology')
            except:
                pass
        return self._technology

if __name__ == '__main__':
    battery = Battery(BATTERY_PATH)
    technology = battery.technology()
    if technology is not None:
        print('Battery ' + technology)
    else:
        print('Battery')
    p = battery.percent()
    v = battery.voltage()
    print(p)
    print(v)
