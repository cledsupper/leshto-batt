from .R import R as Root, join

# Cada conjunto de recursos é relativo ao módulo
class R(Root):
    style = join(Root.path, 'styles.css')
    title = "%s: Monitor da bateria" % (Root.appname)
