#   Leshto Batt batterymonitor.py - Módulo de monitoramento da bateria
#   Copyright (C) 2021-2024 Cledson Ferreira <cledsonitgames@gmail.com>

#    This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# PARA EXECUTAR FACILMENTE
# 1. Abra o diretório pai (src)
# 2. Execute o interpretador Python 3
# 3. Escreva:
# > import ui.batterymonitor
# > ui.batteryMonitor.main()

from threading import Thread, Condition
from batttweaker import LeshtoBatt
from plugin.battlimiter import BatteryChargeLimiter

from gi.repository import GLib
from .module import Module, Gtk
from .res.RBatteryMonitor import R
from pathlib import Path

class BatteryMonitor(Module):
    """Módulo do monitor da bateria"""
    def __init__(self):
        super().__init__(path=R.path, name='battery-monitor')

        st_prov = Gtk.CssProvider()
        Gtk.StyleContext.add_provider_for_screen(
            self.screen,
            st_prov,
            Gtk.STYLE_PROVIDER_PRIORITY_USER
        )
        Gtk.CssProvider.load_from_path(st_prov, R.style)

        self.battery_base = self.builder.get_object('battery-base')
        self.battery_pole = self.builder.get_object('battery-pole')
        self.battery_label = self.builder.get_object('battery-label')
        self.status_level = self.builder.get_object('status-level')
        self.status_charging = self.builder.get_object('status-charging')
        self.status_temp = self.builder.get_object('status-temp')
        self.status_voltage = self.builder.get_object('status-voltage')
        self.status_health = self.builder.get_object('status-health')
        self.limiter_button = self.builder.get_object('limiter-button')
        
        self.mn_global = self.builder.get_object('mn-global')
        self.mn_app_conserv = self.builder.get_object('mn-app-conserv')
        self.mn_app_quit = self.builder.get_object('mn-app-quit')
        self.mn_edit_settings = self.builder.get_object('mn-edit-settings')
        self.mn_view_menu = self.builder.get_object('mn-view-menu')
        self.mn_help_about = self.builder.get_object('mn-app-about')

        self.builder = None

        self.battery = LeshtoBatt()
        self.limiter = BatteryChargeLimiter()

        self.get_status_code(None)

        if Path("/tmp/battlimiter-service.lock").is_file():
          self.limiter_button.set_sensitive(False)
          self.limiter_button.set_tooltip_text('Indisponível quando o serviço de conservação está ativo')

        self.limiter_button.connect('clicked', self.turn_charge_limit)
        
        self.mn_app_conserv.connect('activate', self.turn_charge_limit)
        self.mn_app_quit.connect('activate', quit, self)
        self.mn_view_menu.connect('activate', self.menu_activate)
        self.widget.connect('key_press_event', self.on_alt_key_press)
        self.on_first_iter = True

    def turn_charge_limit(self, _):
        s_code = self.get_status_code(None)
        if s_code < 0:
            print('[E] BatteryMonitor.turn_charge_limit(): dispositivo não suportado!')
            pass
        else:
            if s_code == 0:
                print('[L] BatteryMonitor.turn_charge_limit(): ativando...')
                self.limiter.activate(self.refresh_limiter_button)
            else:
                print('[L] BatteryMonitor.turn_charge_limit(): desativando...')
                self.limiter.deactivate(self.refresh_limiter_button)

    def refresh_limiter_button(self, code):
        GLib.idle_add(
            self.get_status_code,
            code
        )

    def get_status_code(self, code):
        s_code = self.limiter.status() if code is None else code
        if s_code < 0:
            if s_code == -2:
                self.limiter_button.set_label('Configure os limites de carga')
            else:
                self.limiter_button.set_label('Sem suporte')
            self.limiter_button.set_sensitive(False)
        else:
            if s_code == 0:
                self.limiter_button.set_label('Habilitar')
            else:
                self.limiter_button.set_label('Desabilitar')
        self.status_code = s_code
        return s_code

    def menu_activate(self, value):
        print('[L] BatteryMonitor.menu_activate(): barra de menu', end=' ')
        x = not self.mn_global.is_visible()
        self.mn_global.set_visible(x)
        if x == True:
            print('exibida')
        else:
            print('ocultada')

    def on_alt_key_press(self, _w, ev):
        if ev.keyval == 65513:
            print('[L] BatteryMonitor.on_alt_key_press(): tecla ALT ativada')
            self.mn_global.set_visible(True)

    def __daemon(self):
        """Atualizações em segundo plano"""

        def refresh(params = {}):
            """Altera os dados em exibição.
            ### Parâmetros necessários:
             - 'perc': nível da carga bateria em percentil (0-100)
             - 'charging': (True) se está carregando, caso contrário (False)

            ### Parâmetros opcionais (None'áveis):
             - 'unit': unidade dos dados de 'capacity', 'energy_now' e 'current' ((W)att OU (A)mpère).
             - 'energy_now': nível da bateria (em Watt-hora ou Ampère-hora)
             - 'capacity': representa a capacidade total da bateria (em Watt-hora ou Ampère-hora).
             - 'current': velocidade de consumo/carga (em Watt ou Ampère).
             - 'temp': temperatura da bateria.
             - 'voltage': tensão da bateria (em Volts)
             - 'health': saúde da bateria
            """
            perc = params['perc']
            charging = params['charging']
            unit = params['unit']
            capacity = params['capacity']
            current = params['current']
            energy_now = params['energy_now']
            temp = params['temp']
            voltage = params['voltage']
            health = params['health']
            
            # desenho da bateria
            strperc = str(perc) + ' %'
            self.battery_base.set_value(perc - 1 if perc > 0 else 0)
            self.battery_pole.set_value(0 if perc < 100 else 1)
            self.battery_label.set_text(strperc)
            
            # status
            strcharging = 'Sim' if charging else 'Não'
            if current:
                self.status_charging.set_text('%s - %0.2f %s' % (strcharging, current, unit))
            else:
                self.status_charging.set_text(strcharging)
            
            has_capacity = capacity is not None
            has_energy_now = energy_now is not None
            if has_capacity:
                fmtpars = (
                    (energy_now, capacity, unit) if has_energy_now
                    else (perc*capacity/100, capacity, unit)
                )
                self.status_level.set_text('%0.1f/%0.1f %sh' % fmtpars)
            else:
                self.status_level.set_text(strperc)
            
            if temp is not None:
                self.status_temp.set_text('%d ºC' % (temp))
            if voltage is not None:
                self.status_voltage.set_text('%.2f V' % (voltage))
            if health is not None:
                self.status_health.set_text('%s' % (health))

            if self.on_first_iter:
                self.mn_global.set_visible(False)
                self.on_first_iter = False
        
        # Lê o status da bateria e mostra
        self.t_cond.acquire()
        while True:
            if self.battery.check_settings(): self.battery.load_settings()
            GLib.idle_add(
                refresh,
                {
                    'perc': self.battery.percent(),
                    'charging': self.battery.charging(),
                    'unit': self.battery.get_unit(),
                    'energy_now': self.battery.energy_now(),
                    'capacity': self.battery.capacity(),
                    'current': self.battery.current_now(),
                    'temp': self.battery.temp(),
                    'voltage': self.battery.voltage(),
                    'health': self.battery.health()
                }
            )
            # Verifica o sinal da thread principal para encerrar
            if self.t_cond.wait(timeout=1): break
        self.t_cond.release()

    def run(self):
        """Executa as tarefas do módulo"""
        # t_cond é um sinalizador  para avisar a thread quando
        # ... for necessário encerrar imediatamente
        self.t_cond = Condition()
        self.thread = Thread(
            target=self.__daemon,
            daemon=True)
        self.thread.start()

    def stop(self):
        """Encerra as tarefas do módulo"""
        self.t_cond.acquire()
        self.t_cond.notify()
        self.t_cond.release()
        self.thread.join()
        self.t_cond = None
        self.thread = None

# NÃO CHAME m.quit NO CÓDIGO DO MÓDULO PRINCIPAL!
# O módulo principal deve chamar m.stop() ao encerrar
def quit(widget, module):
    module.stop()
    Gtk.main_quit()

# NÃO EXECUTE main NO CÓDIGO DO MÓDULO PRINCIPAL!
# O módulo principal deve incluir e mostrar m.widget, e executar
# ... o módulo com m.run_threads()
def main():
    module = BatteryMonitor()
    
    window = Gtk.Window(title=R.title)
    window.add(module.widget)
    window.connect('destroy', quit, module)
    window.show_all()
    
    module.run()
    Gtk.main()
