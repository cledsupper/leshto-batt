#!/bin/sh

[ $(id -u) -ne 0 ] && exit 1

UP_FILE="/tmp/battlimiter-service.lock"
if [ -e "$UP_FILE" ]; then
  echo "The service is already running. Stop it and try again!"
  exit 2
fi
touch $UP_FILE

# executa o primeiro argumento e salva o código de erro no segundo argumento (variável)
_() {
eval $1
read $2 <<EOF
$?
EOF
eval $2=\$$2
}

MAX=80
MIN=70

get_percent() {
  p=`cat "/sys/class/power_supply/BAT0/capacity"`
  return $p
}

disable_charge() {
  [ $stop_status -eq 1 ] && return 0
  echo "Disabling the charge"
  _ "battlimiter-iface.sh -e" e
  [ $e -eq 0 ] && stop_status=1
  return $e
}

allow_charge() {
  [ $stop_status -eq 0 ] && return 0
  echo "Allowing to charge"
  _ "battlimiter-iface.sh -d" e
  [ $e -eq 0 ] && stop_status=0
  return $e
}

stop_status=-1
refresh_status() {
  _ "battlimiter-iface.sh -s" stop_status
}

percent=-1
while [ -e "$UP_FILE" ]; do
  _ get_percent percent
  refresh_status
  if [ $stop_status -ne 1 ]; then
    [ $percent -ge $MAX ] && [ $percent -lt 95 ] && disable_charge
  else
    [ $percent -lt $MIN ] && allow_charge
  fi
  sleep 1
done

echo "Terminating LeshtoBatt service"
allow_charge
