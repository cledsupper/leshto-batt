#   Leshto Batt battlimiter.py - Interface para o controle da carga da bateria.
#   Copyright (C) 2024 Cledson Ferreira <cledsonitgames@gmail.com>

#    This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

import subprocess

IFACE_CMD = "battlimiter-iface.sh"
def sudo_iface(arg):
    if iface() == -127:
        return -127
    code = subprocess.call(["pkexec", IFACE_CMD, arg])
    return code
 
def iface():
    try:
        code = subprocess.call([IFACE_CMD, "-s"])
        return code
    except:
        return -127

class BatteryChargeLimiter():
    def status(self):
        """Retorna o estado do modo de conservação da bateria.
        
        Valores possíveis:
         -> (-1): nenhum suporte ou plugin incompatível.
         -> (-2): os limites de carga devem ser configurados na interface.
         -> (0): desativado.
         -> (1): limitador de bateria ativa.
         -> (?): erro de autenticação.
        """
        code = iface()
        return code

    def activate(self, cb_when_performed):
        code = sudo_iface("-e")
        if code == 0:
            cb_when_performed(1)
            return True
        return False

    def deactivate(self, cb_when_performed):
        code = sudo_iface("-d")
        if code == 0:
            cb_when_performed(0)
        return False
